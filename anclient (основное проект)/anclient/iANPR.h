/******************************************************************************
	(C) 2013. IntBuSoft. http://intbusoft.com
	iANPR.h - version 1.1		
*******************************************************************************/
#pragma once

#include "opencv2/core/core_c.h"
#include "opencv2/imgproc/imgproc_c.h"

// ������ �������������� �������������� ������
// ������, ���������� �� �������������� ������ �������
#define ANPR_DETECTMODE1			0x01  // ������� ���������� ���������
#define ANPR_DETECTMODE2			0x02  // ���������� ��������� � �������������� ����������� �����������
#define ANPR_DETECTMODE3			0x04  // ������� ���������� ���������
// ������, ���������� �� ��������� ������ ������
#define ANPR_DETECTMODE4			0x08  // ������� ���������� ��������� � ���������� ��������	������

#define ANPR_DETECTCOMPLEXMODE		ANPR_DETECTMODE2 | ANPR_DETECTMODE3 | ANPR_DETECTMODE4

// ���� �������������� ������������� �������
#define	ANPR_RUSSIAN_BASE			0x00  // �������
#define	ANPR_RUSSIAN_BASE2			0x01  // ������� � ����������
#define	ANPR_RUSSIAN_EXTENDED		0x02  // �������, ���������� � ������ ��������

// ���������� flags � ANPR_OPTIONS
#define DEBUG_RECOGNITION_MODE		0x01  // ����� ���������� ��� ������������ ������, � ��� ����� � � ��������������� ��������� (���� �������)

struct ANPR_OPTIONS
{
	int min_plate_size; // ����������� ������� ������
	int max_plate_size; // ������������ ������� ������
	int Detect_Mode; // ������ ��������������	
	int max_text_size; // ������������ ���������� �������� ������ + 1
	int type_number; // ��� ��������������� ������ �����	
	int flags; // �������������� �����
};

int
#ifdef WIN32
__stdcall 
#endif 
anprPlate( IplImage* Image8, ANPR_OPTIONS Options, int* AllNumber, CvRect* Rects, char** Texts );

int
#ifdef WIN32
__stdcall 
#endif 
anprPlateRect( IplImage* Image8, CvRect Rect, ANPR_OPTIONS Options, int* AllNumber, CvRect* Rects, char** Texts );

int
#ifdef WIN32
__stdcall 
#endif 
anprPlateMemory( char* in_buffer, int size_buffer, ANPR_OPTIONS Options, int* AllNumber, CvRect* Rects, char** Texts );

int
#ifdef WIN32
__stdcall 
#endif 
anprPlateMemoryRect( char* in_buffer, int size_buffer, CvRect Rect, ANPR_OPTIONS Options, int* AllNumber, CvRect* Rects, char** Texts );