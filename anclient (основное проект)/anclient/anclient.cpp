#include <winsock2.h>
#include <ws2tcpip.h>

#include <opencv2/imgproc/imgproc_c.h>
#include <opencv2/highgui/highgui_c.h>

#include <stdio.h>
#include <Windows.h>
#include <time.h> 

#include "iANPR.h"

using namespace std;

#pragma comment (lib, "Ws2_32.lib")
#pragma comment (lib, "Mswsock.lib")
#pragma comment (lib, "AdvApi32.lib")


#define DEFAULT_BUFLEN 512
#define DEFAULT_PORT "27015"

CvCapture* capture;
int all = 100;
CvRect Rects[100];				
char** res;
char outbuf[256];
bool stop;
CRITICAL_SECTION cs;

void get_result( char* dst )
{
	strcpy_s( dst, 256, outbuf );
}

CRITICAL_SECTION* Section_()
{
	return &cs;
}

int send_socket(char* sendbuf)
{
	WSADATA wsaData;
	SOCKET ConnectSocket = INVALID_SOCKET;
	struct addrinfo *result = NULL,
		*ptr = NULL,
		hints;

	char recvbuf[DEFAULT_BUFLEN];
	int iResult;
	int recvbuflen = DEFAULT_BUFLEN;

	char* _addr;

	_addr = "127.0.0.1";

	// Initialize Winsock
	iResult = WSAStartup(MAKEWORD(2,2), &wsaData);
	if (iResult != 0) {
		printf("WSAStartup failed with error: %d\n", iResult);
		return 1;
	}

	ZeroMemory( &hints, sizeof(hints) );
	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = IPPROTO_TCP;

	// Resolve the server address and port
	//iResult = getaddrinfo(argv[1], DEFAULT_PORT, &hints, &result);
	iResult = getaddrinfo(_addr, DEFAULT_PORT, &hints, &result);
	if ( iResult != 0 ) {
		printf("getaddrinfo failed with error: %d\n", iResult);
		WSACleanup();
		return 1;
	}

	// Attempt to connect to an address until one succeeds
	for(ptr=result; ptr != NULL ;ptr=ptr->ai_next) {

		// Create a SOCKET for connecting to server
		ConnectSocket = socket(ptr->ai_family, ptr->ai_socktype, 
			ptr->ai_protocol);
		if (ConnectSocket == INVALID_SOCKET) {
			printf("socket failed with error: %ld\n", WSAGetLastError());
			WSACleanup();
			return 1;
		}

		// Connect to server.
		iResult = connect( ConnectSocket, ptr->ai_addr, (int)ptr->ai_addrlen);
		if (iResult == SOCKET_ERROR) {
			closesocket(ConnectSocket);
			ConnectSocket = INVALID_SOCKET;
			continue;
		}
		break;
	}

	freeaddrinfo(result);

	if (ConnectSocket == INVALID_SOCKET) {
		printf("Unable to connect to server!\n");
		WSACleanup();
		return 1;
	}

	// Send an initial buffer
	iResult = send( ConnectSocket, sendbuf, (int)strlen(sendbuf), 0 );
	if (iResult == SOCKET_ERROR) {
		printf("send failed with error: %d\n", WSAGetLastError());
		closesocket(ConnectSocket);
		WSACleanup();
		return 1;
	}

	printf("Bytes Sent: %ld\n", iResult);

	// shutdown the connection since no more data will be sent
	iResult = shutdown(ConnectSocket, SD_SEND);
	if (iResult == SOCKET_ERROR) {
		printf("shutdown failed with error: %d\n", WSAGetLastError());
		closesocket(ConnectSocket);
		WSACleanup();
		return 1;
	}

	// Receive until the peer closes the connection
	do {

		iResult = recv(ConnectSocket, recvbuf, recvbuflen, 0);
		if ( iResult > 0 )
			printf("Bytes received: %d\n", iResult);
		else if ( iResult == 0 )
			printf("Connection closed\n");
		else
			printf("recv failed with error: %d\n", WSAGetLastError());

	} while( iResult > 0 );

	// cleanup
	closesocket(ConnectSocket);
	WSACleanup();
}

DWORD WINAPI ImageProcess(LPVOID)
{

	ANPR_OPTIONS a;		
	a.Detect_Mode = ANPR_DETECTCOMPLEXMODE;
	a.min_plate_size = 500;
	a.max_plate_size = 25000;
	a.max_text_size = 20;		
	a.type_number = ANPR_RUSSIAN_BASE2;				
	a.flags = 0;

	CvSize Size = cvSize( 320, 240 );
	IplImage* small_image = cvCreateImage( Size, 8, 3 );
	IplImage* object = 0;

	char mem[100][20];
	int all_mem = 0;

	for(;;)
	{
		IplImage* frame = 0;    
		frame = cvQueryFrame(capture);
		if( !frame )
			break;		
		if (!object)
		{
			object = cvCreateImage(cvGetSize(frame), 8, 1);			
			cvZero( object );
			object->origin = frame->origin;				
		}
		cvCvtColor( frame, object, CV_BGR2GRAY );
		cvSaveImage( "test.jpg", object );		
		all = 100;

		int i1;
		__try
		{
			i1 = anprPlate( object, a, &all, Rects, res ); 
		}
		__except(EXCEPTION_EXECUTE_HANDLER)
		{
			ExitProcess( 1 );
		}

		int number = 0;
		if ( i1 == 0 )
		{						
			for(int j = 0; j < all; j++ )				
			{				
				if ( strlen( res[j] ) >= 8 )
				{
					int k =0;
					for( int j1 = 0; j1 < all_mem; j1++ )
						if ( strcmp( res[j], mem[j1]) == 0 ) k = 1;
					if ( k == 0 ) continue;		
					cvRectangle( frame,cvPoint( Rects[j].x, Rects[j].y),cvPoint(Rects[j].x+Rects[j].width,
						Rects[j].y+Rects[j].height), CV_RGB(255,255,0), 2);

					CvFont font;
					float aa=0.001f*frame->width;
					cvInitFont( &font, CV_FONT_HERSHEY_SIMPLEX, aa,
						aa,0,1, 8 ); 
					CvPoint pp2,pp1;
					pp2.x=Rects[j].x;
					pp2.y=Rects[j].y;
					pp1.x=Rects[j].x+1;
					pp1.y=Rects[j].y+1;
					cvPutText( frame, res[j], pp1, &font, CV_RGB(0,0,0) );
					cvPutText( frame, res[j], pp2, &font, CV_RGB(0,255,0) );
				}			
			}
			for(int j = 0; j < all; j++ )				
			{
				for( i1 = 0; i1 < strlen( res[j] ); i1++ )
					if ( res[j][i1] == '?' ) {
						i1 = -1;
						break;
					}
					if ( i1 == -1 ) continue;
					if ( strlen( res[j] ) >= 8 )
					{
						strcpy( mem[j], res[j] );
					}
			}
			all_mem = all;
		}		

		cvResize( frame, small_image );

		cvShowImage("frame", frame);

		EnterCriticalSection(&cs);

		for(int j = 0; j < all; j++ )
		{
			time_t rawtime; 
			struct tm * timeinfo; 
			time ( &rawtime ); 
			timeinfo = localtime ( &rawtime ); 

			sprintf( outbuf,"Time and data: %s - Number: %s\n", asctime (timeinfo), res[j] ); 

			printf(outbuf);

			send_socket(res[j]);
		}

		LeaveCriticalSection(&cs);		
		Sleep( 2 ); 
		if ( stop ) break;
	}

	cvReleaseCapture( &capture );
	cvReleaseImage( &small_image );
	cvReleaseImage( &object );
	DeleteCriticalSection(&cs);


	for(int j=0;j<100;j++) delete [] res[j];
	delete [] res;

	return 0;
}

int start_process(char* IP)
{
	capture = cvCaptureFromCAM(0);
	if ( capture == NULL ) return 1;
	
	cvNamedWindow("frame",CV_WINDOW_AUTOSIZE);

	DWORD lpT;
	stop = false;
	InitializeCriticalSection(&cs);
	res = new char*[all];
	for(int j=0;j<all;j++) res[j] = new char[20];

	CreateThread( NULL,0,ImageProcess,NULL,0,&lpT); 

	return 0;
}

void stop_process()
{
	stop = true;

	cvDestroyAllWindows();
}

int main(int argc, char* argv[])
{

	start_process("127.0.0.1");

	while(1)
	{
		char k = cvWaitKey(2);

		if (k == 'ESC')
		{
			stop_process();
			return 0;
		}
	}
	

	return 0;
}